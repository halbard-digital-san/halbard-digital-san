# [S Naude](https://gitlab.com/halbard-digital-san/)

> "Seven days without coding and life is meaningless."
> &emsp;&mdash; __The Tao of Programming__ (1987); Geoffrey James

For details about me, please see the sidebar.

For my Work Portfolio, please visit [my Website](https://www.halbard-digital-softwr.co.za/portfolio.php) (currently being reworked to use an MVC framework: Laravel/Symfony).

## Technology Stack

As mentioned previously, I have over a decade's worth of work experience with the following technologies (and a number of related ones):

- Alpaca.js Forms
- Bourne-Again Shell (`bash`) & GNU/Linux (Arch/Manjaro, Debian, Fedora, TAILS &amp; Ubuntu), with `vim`
- Bootstrap
- C# and ASP .Net MVC
- CSS, [X]HTML5, ECMA/JavaScript &amp; jQuery
- DataTables.js
- GNU Image Manipulation Program (GIMP)
- Gridstack.js
- Handlebars/Moustache
- Java (including DBs/ODBC, jCIFS & SMB, JSPs, servlets and RS 232 serial communications)
- LAMP &amp; Drupal
- Libre/OpenOffice
- Lodash/Underscore
- MS Access/uCanAccess (Java library)
- MS Office
- MySQL
- [Nim](https://nim-lang.org/) (like Python, but with strong typing and transpilation to C/C++/JS)
- Python
- REST APIs (including AJAX, JSON & XML)
- Require.js
- Search Engine Optimisation (SEO)
- SQLite
- TCP/IP sockets (in Java)
- WebSockets (in Java and JavaScript)

## Contact Me

Please [contact me, with an appropriate subject ("Quote"), for a quote](mailto:halbard.digital.sw{.nospam}@protonmail.com?subject=Quote) if you are looking for a backend/frontend/full-stack Web developer or would like me to do one (or more) of the following tasks, as either a long-term (months to years) or short-term project/job:

- Blogging
- Data capture
- Copy/Ghost writing (especially, but not limited to, technical/technology). I also write short stories.
- Research (particularly development frameworks/technologies)
- Set up a cloud/remote Compute instance (LAMP/LEMP stack, optionally including WordPress or Drupal)
- Software/Web development (Bootstrap, CSS &amp; [X]HTML5; C#/Java/PHP/Python)
- Technical support (software)
- Dealing with customer service/tech support/developers on your behalf

For some unknown reason, I cannot set [my Mastodon username](@greatwhitesnark@mastodon.online) in my GitLab profile, so I have put [it](https://mastodon.online/@greatwhitesnark) in this file.

## Freelance Rates

I charge a *daily* (8 hours) rate of $47.75 USD (~$6.00 USD per hour) for development work. If a job takes me less than an hour, I charge for a full hour. I charge per fifteen minutes thereafter.

<details>
  <summary>Caveat Emptor (or maybe it's Lector?)</summary>
  <p>I'm an arrogant and stubborn bastard with a massive ego and superiority complex. Why? Because if you make a problem interesting and challenging enough, I will channel that stubbornness into perseverance, will not give up trying to solve it, no matter how many attempts required to make the solution perfect, including temporarily giving up my own health and sleep schedule. (I am a perfectionist, to a fault.) I take introspection seriously. (I haven't yet mastered all of Wall's three virtues of a great programmer, but I am working on it.)</p><p>I'm good at debugging terrible code because I've writ so much of it and learned from it. YMMV.</p>
  <p>I deliver software on an as-ready basis, subject to Hofstadter's Law and Cyril Parkinson's Law. Estimates are given in [Valve Corporation Time](https://developer.valvesoftware.com/wiki/Valve_Time).</p>
</details>
